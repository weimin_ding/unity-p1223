﻿using System;

[Serializable]
public class ReturnCode
{
    public string id;
    public long seq;
    public int code; //200 is OK, 4XX and 5XX as Error code
}
