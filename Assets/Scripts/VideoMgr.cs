﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoMgr : MonoBehaviour
{
    [SerializeField]
    GameObject CameraImage;

    [SerializeField]
    GameObject password;

    [SerializeField]
    bool isSetPass;

    bool isSetLight;

    int errorCount = 0;

    [SerializeField]
    VideoClip endVideo;

    [SerializeField]
    Material door1, door2;

    [SerializeField]
    SkinnedMeshRenderer door;

    [SerializeField]
    Camera point_chuihuiwc, point_prassword,v1,v2;

    int textIndex;

    [SerializeField]
    VideoPlayer videoPlayer;
    [SerializeField]
    Camera mainCamera;
    [SerializeField]
    GameObject videoUI,arrowUI;
    
    [SerializeField]
    GameObject senceObj;

    [SerializeField]
    Animator animator;

    [SerializeField]
    CameraMgr cameraMgr;

    [SerializeField]
    TextMesh passText1,passText2,passText3,passText4;

    List<string> passTexts;

    bool isPaused;

    bool isVideoFinished;

    float clickTime = 0;

    bool isMouseDown = false;

    float c_x,l_x;
    float c_y,l_y;

    float timer = 0;

    float CD = 0.4f;

    public float gameTime = 0;

    bool isInKeyEventCD = false;

    Vector3 camAng;

    [SerializeField]
    GameObject light;

    [SerializeField]
    MicrophoneInput microphoneInput;

    [SerializeField]
    VideoClip[] videoClips;

    [SerializeField]
    VideoClip[] videoClips_2;

    [SerializeField]
    CharacterMgr characterMgr;

    [SerializeField]
    int index;

    public bool canMicrophoneInput;

    public bool canTextInput;

    public Text helpText;

    //bool passwordLock;

    public bool canChangeDoor;

    [SerializeField]
    GameObject resetButton;

    public bool isFinishLightEvent;
         
    public int videoClipsIndex = 0;

    public bool isOpenedLight;


    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        Application.targetFrameRate = 24;
        videoPlayer.loopPointReached += OnLoopPointReached;
        cameraMgr.ShowCamera(0,index);
        animator.speed = 0f;
        senceObj.SetActive(false);
    }

    void Update()
    {
        if ((videoPlayer.frame >= 421 && index == 1) || ((videoPlayer.frame >= 151 && videoPlayer.frame < 350) || (videoPlayer.frame >= 371 && videoPlayer.frame < 613)) && index == 3)
        {
            CameraImage.SetActive(true);
        }
        else
            CameraImage.SetActive(false);

        if (index > 3 && !isSetPass)
            return;

        if (index > 2 && !isSetLight)
            return;

        //if (passwordLock)
        //    return;

            if (isInKeyEventCD)
        {
            if (timer < CD)
                timer += Time.deltaTime;
            else
            {
                timer = 0;
                isInKeyEventCD = false;
            }
            return;
        }

        if (isPaused || isVideoFinished)
        {
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
                ZoomIn();

            else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
                ZoomOut();

            if (Input.GetMouseButton(0))
            {
                MouseDown();
            }

            if (Input.GetMouseButtonUp(0))
            {
                MouseUp();
            }

            if (clickTime > 0.3f)
            {
                isMouseDown = true;          
            }

            if (isMouseDown)
            {
                float y = Input.GetAxis("Mouse X") * 2f;
                //float x = Input.GetAxis("Mouse Y");
                //x -=  (c_y - l_y) * 0.3f;
                y +=  (c_x - l_x) * 0.3f;
                //camAng.x -= x;
                 camAng.y += y;
                //l_x = c_x;
                l_y = c_y;
                mainCamera.transform.eulerAngles = camAng;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                OpenOrCloseLight();
            }

            if (isVideoFinished)
                return;


            if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.RightArrow))
                PlayVideo();
        }
        else
        {
            gameTime += Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow)
                || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
            {
                PauseVideo();
            }

            //if (gameTime > 61f && gameTime < 65f)
            //{
            //    if (!helpText.gameObject.activeSelf)
            //        helpText.gameObject.SetActive(true);
            //    helpText.text = "按下暂停键，可以通过手机麦克风吹掉盒子上的灰尘！";
            //}
            //else if (gameTime > 85f && gameTime < 89f)
            //{
            //    if (!helpText.gameObject.activeSelf)
            //        helpText.gameObject.SetActive(true);
            //    helpText.text = "通过手机输入密码打开盒子！";
            //}
            //else if (gameTime > 95f && gameTime < 99f)
            //{
            //    if (!helpText.gameObject.activeSelf)
            //        helpText.gameObject.SetActive(true);
            //    helpText.text = "暂停后可以通过功能键切换门材质！";
            //}
            //else
            //{
            //    if (helpText.gameObject.activeSelf)
            //        helpText.gameObject.SetActive(false);
            //}
        }
    }

    public void Reset()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void SetVolume(float value)
    {
        microphoneInput.SetVolume(value);
    }

    public IEnumerator ChangePasswordLock()
    {
        yield return new WaitForSeconds(1.2f);
        index++;
        videoClipsIndex = 1;
        videoPlayer.clip = videoClips_2[index];
        videoPlayer.Play();
        characterMgr.NextCharacter();
        cameraMgr = characterMgr.currentCharacter.GetComponent<CameraMgr>();
        animator = cameraMgr.GetComponent<Animator>();
        animator.speed = 0f;
        videoUI.SetActive(true);
        isPaused = false;
        senceObj.SetActive(false);
        helpText.gameObject.SetActive(false);
        isSetPass = true;

    }

    public void OpenOrCloseLight()
    {
        if (isInKeyEventCD)
            return;

        else if (characterMgr.index == 1 && cameraMgr.index == 5)
        {

            if (mainCamera.transform.position != v1.transform.position)
            {
                mainCamera.transform.position = v1.transform.position;
                mainCamera.transform.rotation = v1.transform.transform.rotation;
                mainCamera.fieldOfView = v1.fieldOfView;
                mainCamera.nearClipPlane = v1.nearClipPlane;
                mainCamera.farClipPlane = v1.farClipPlane;
                mainCamera.GetComponent<PostProcessingBehaviour>().profile = v1.GetComponent<PostProcessingBehaviour>().profile;
            }
            else
            {
                mainCamera.transform.position = cameraMgr.currentCamrea.transform.position;
                mainCamera.transform.rotation = cameraMgr.currentCamrea.transform.rotation;
                mainCamera.fieldOfView = cameraMgr.currentCamrea.GetComponent<Camera>().fieldOfView;
                mainCamera.nearClipPlane = cameraMgr.currentCamrea.GetComponent<Camera>().nearClipPlane;
                mainCamera.farClipPlane = cameraMgr.currentCamrea.GetComponent<Camera>().farClipPlane;
                mainCamera.GetComponent<PostProcessingBehaviour>().profile = cameraMgr.currentCamrea.GetComponent<PostProcessingBehaviour>().profile;
            }

        }
        else if (characterMgr.index == 3 && (cameraMgr.index == 3 || cameraMgr.index == 5))
        {

            if (mainCamera.transform.position != v2.transform.position)
            {
                mainCamera.transform.position = v2.transform.position;
                mainCamera.transform.rotation = v2.transform.transform.rotation;
                mainCamera.fieldOfView = v2.fieldOfView;
                mainCamera.nearClipPlane = v2.nearClipPlane;
                mainCamera.farClipPlane = v2.farClipPlane;
                mainCamera.GetComponent<PostProcessingBehaviour>().profile = v2.GetComponent<PostProcessingBehaviour>().profile;
            }
            else
            {
                mainCamera.transform.position = cameraMgr.currentCamrea.transform.position;
                mainCamera.transform.rotation = cameraMgr.currentCamrea.transform.rotation;
                mainCamera.fieldOfView = cameraMgr.currentCamrea.GetComponent<Camera>().fieldOfView;
                mainCamera.nearClipPlane = cameraMgr.currentCamrea.GetComponent<Camera>().nearClipPlane;
                mainCamera.farClipPlane = cameraMgr.currentCamrea.GetComponent<Camera>().farClipPlane;
                mainCamera.GetComponent<PostProcessingBehaviour>().profile = cameraMgr.currentCamrea.GetComponent<PostProcessingBehaviour>().profile;
            }

        }
        else if (canChangeDoor)

        {
            if (isPaused || isVideoFinished)
            {
                isInKeyEventCD = true;
                if (door.sharedMaterial == door2)
                    door.sharedMaterial = door1;
                else
                    door.sharedMaterial = door2;

            }
        }
        else
        {
            if (isFinishLightEvent)
                return;

            if (index < 1)
                return;

            if (isPaused || isVideoFinished)
            {
                if (!light.activeSelf)
                    light.SetActive(true);
                else
                    light.SetActive(false);
                isInKeyEventCD = true;
            }
        }
    }

    public void PlayOrPauseVideo()
    {
        if (isInKeyEventCD)
            return;

        if (isVideoFinished)
            return;

        if (isPaused)
            PlayVideo();
        else
            PauseVideo();
        isInKeyEventCD = true;
    }

    public void PlayVideo()
    {
        if (index == 1 && cameraMgr.index == 0)
        {
            if (light.activeSelf)
            {
                videoPlayer.clip = videoClips_2[index];
                videoClipsIndex = 1;
                videoPlayer.time = 3.5f;
                isSetLight = true;
            }
            isFinishLightEvent = true;
        }
        videoUI.SetActive(true);

        videoPlayer.Play();
        isPaused = false;
        senceObj.SetActive(false);
        if (helpText.gameObject.activeSelf)
            helpText.gameObject.SetActive(false);
        if (microphoneInput.audioSource.isPlaying)
            microphoneInput.audioSource.Stop();


    }

    public void MouseUp()
    {
        if (isPaused || isVideoFinished)
        {
            clickTime = 0;
            isMouseDown = false;
            camAng.x = mainCamera.transform.eulerAngles.x;
            camAng.y = mainCamera.transform.eulerAngles.y;
        }
    }

    public void MouseDown()
    {
        if (isPaused || isVideoFinished)
        {
            clickTime += Time.deltaTime;
            camAng.x = mainCamera.transform.eulerAngles.x;
            camAng.y = mainCamera.transform.eulerAngles.y;
        }
    }

    public void ZoomIn()
    {
        if (isPaused || isVideoFinished)
        {
            if (mainCamera.fieldOfView > 30)
                mainCamera.fieldOfView -= Time.deltaTime * 50f;
            else
                mainCamera.fieldOfView = 30;
        }
    }

    public void ZoomOut()
    {
        if (isPaused || isVideoFinished)
        {
            if (mainCamera.fieldOfView < 80)
                mainCamera.fieldOfView += Time.deltaTime * 50f;
            else
                mainCamera.fieldOfView = 80;
        }
    }

    public void IntMontion(float _x, float _y)
    {
        c_x = l_x = _x;
        c_y = l_y = _y;
    }

    public void SetMontion(float _x,float _y)
    {
        c_x = _x;
        c_y = _y;
    }

    public void SetText(string text)
    {
        if (textIndex == 0)
        {
            passText1.gameObject.SetActive(true);
            passText2.gameObject.SetActive(false);
            passText3.gameObject.SetActive(false);
            passText4.gameObject.SetActive(false);
            passText1.text = text;
        }
        else if (textIndex == 1)
        {
            passText2.gameObject.SetActive(true);
            passText2.text = text;
        }
        else if (textIndex == 2)
        {
            passText3.gameObject.SetActive(true);
            passText3.text = text;
        }
        else if (textIndex == 3)
        {
            passText4.gameObject.SetActive(true);
            passText4.text = text;
        }

        textIndex++;
        if (textIndex == 4)
        {
            textIndex = 0;

            string textPass = passText1.text + passText2.text + passText3.text + passText4.text;

            Debug.Log(textPass);
            if (textPass == "1220")
            {
                StartCoroutine(ChangePasswordLock());
            }
            else
            {
                helpText.gameObject.SetActive(true);
                helpText.text = "密码输入错误，正确密码是1220！";
                //StartCoroutine(LaterHideText());
                errorCount++;
                if (errorCount > 2)
                {
                    helpText.gameObject.SetActive(false);
                    videoUI.SetActive(true);
                    isPaused = false;
                    senceObj.SetActive(false);
                    videoPlayer.clip = endVideo;
                    videoPlayer.Play();

                }
            }
        }

    }


    public void ShowMessage(string text)
    {
        Debug.Log(text);
        passTexts = new List<string>();
        foreach (char c in text)
        {
            passTexts.Add(c.ToString());
        }

        if(passTexts.Count > 0)
            passText1.text = passTexts[0];
        if (passTexts.Count > 1)
            passText2.text = passTexts[1];
        if (passTexts.Count > 2)
            passText3.text = passTexts[2];
        if (passTexts.Count > 3)
            passText4.text = passTexts[3];

        if (text == "1220" )
        {
            StartCoroutine(ChangePasswordLock());
        }
        else
        {
            //helpText.gameObject.SetActive(true);
            //helpText.text = "密码输入错误，正确密码是1220！";
            //StartCoroutine(LaterHideText());
            videoUI.SetActive(true);
            isPaused = false;
            senceObj.SetActive(false);
            videoPlayer.clip = endVideo;
            videoPlayer.Play();
        }

    }

    IEnumerator LaterPauseVideo()
    {
        yield return new WaitForSeconds(4.1f);
        PauseVideo();
    }

    IEnumerator LaterHideText()
    {
        yield return new WaitForSeconds(3f);
        helpText.gameObject.SetActive(false);
    }



    public void PauseVideo()
    {
        if (index > 0 && !light.activeSelf && videoClipsIndex == 0 && isFinishLightEvent)
            return;

        senceObj.SetActive(true);
        cameraMgr.ShowCamera((int)videoPlayer.frame, index);
        animator.Play("Ani", -1, (float)((double)videoPlayer.frame / (double)videoPlayer.frameCount));

        if (characterMgr.index == 1 && cameraMgr.index == 0)
        {
            canMicrophoneInput = false;
            microphoneInput.canUse = false;
            mainCamera.transform.position = cameraMgr.currentCamrea.transform.position;
            mainCamera.transform.rotation = cameraMgr.currentCamrea.transform.rotation;
            mainCamera.fieldOfView = cameraMgr.currentCamrea.GetComponent<Camera>().fieldOfView;
            mainCamera.nearClipPlane = cameraMgr.currentCamrea.GetComponent<Camera>().nearClipPlane;
            mainCamera.farClipPlane = cameraMgr.currentCamrea.GetComponent<Camera>().farClipPlane;
            mainCamera.GetComponent<PostProcessingBehaviour>().profile = cameraMgr.currentCamrea.GetComponent<PostProcessingBehaviour>().profile;
          
        }

      

        else if (characterMgr.index == 2 && cameraMgr.index == 6)
        {
            canMicrophoneInput = true;
            microphoneInput.canUse = true;
            mainCamera.transform.position = point_chuihuiwc.transform.position;
            mainCamera.transform.rotation = point_chuihuiwc.transform.rotation;
            mainCamera.fieldOfView = point_chuihuiwc.fieldOfView;
            mainCamera.nearClipPlane = point_chuihuiwc.nearClipPlane;
            mainCamera.farClipPlane = point_chuihuiwc.farClipPlane;

        }
        else if (characterMgr.index == 3 && cameraMgr.index == 6)
        {
            Debug.Log("111");
            canMicrophoneInput = true;
            microphoneInput.canUse = true;
            mainCamera.transform.position = point_prassword.transform.position;
            mainCamera.transform.rotation = point_prassword.transform.rotation;
            mainCamera.fieldOfView = point_prassword.fieldOfView;
            mainCamera.nearClipPlane = point_prassword.nearClipPlane;
            mainCamera.farClipPlane = point_prassword.farClipPlane;
            password.gameObject.SetActive(true);
        }
        else
        {
            if(helpText.gameObject.activeSelf)
            helpText.gameObject.SetActive(false);
            canMicrophoneInput = false;
            microphoneInput.canUse = false;
            mainCamera.transform.position = cameraMgr.currentCamrea.transform.position;
            mainCamera.transform.rotation = cameraMgr.currentCamrea.transform.rotation;
            mainCamera.fieldOfView = cameraMgr.currentCamrea.GetComponent<Camera>().fieldOfView;
            mainCamera.nearClipPlane = cameraMgr.currentCamrea.GetComponent<Camera>().nearClipPlane;
            mainCamera.farClipPlane = cameraMgr.currentCamrea.GetComponent<Camera>().farClipPlane;
            mainCamera.GetComponent<PostProcessingBehaviour>().profile = cameraMgr.currentCamrea.GetComponent<PostProcessingBehaviour>().profile;
            password.gameObject.SetActive(false);
        }
        isPaused = true;
        cameraMgr.currentCamrea.SetActive(false);
        videoPlayer.Pause();
        videoUI.SetActive(false);
        //arrowUI.SetActive(true);
    }

    private  void OnLoopPointReached(VideoPlayer videoPlayer)
    {


        if (index == 3 && !isSetPass)
        {
            videoClipsIndex = 0;
            Debug.Log("233");
        }
        else if (index > 4 && !isSetPass)
        {
            videoUI.transform.GetChild(0).gameObject.SetActive(false);
            resetButton.SetActive(true);
            return;
        }

        int endCount = videoClips.Length - 1;
        if(videoClipsIndex == 1)
            endCount = videoClips_2.Length - 1;
        if (index < endCount)
        {
            //if (characterMgr.index == 3)
            //    SetPasswordLock();

            //else
            //{
                index++;
        

            if (videoClipsIndex == 0)
                    videoPlayer.clip = videoClips[index];
                else
                    videoPlayer.clip = videoClips_2[index];
                videoPlayer.Play();
                characterMgr.NextCharacter();
                cameraMgr = characterMgr.currentCharacter.GetComponent<CameraMgr>();
                animator = cameraMgr.GetComponent<Animator>();
                animator.speed = 0f;
                //if (characterMgr.index == 1)
                //    StartCoroutine(LaterPauseVideo());
            //}
        }
        else
        {
            isVideoFinished = true;
            resetButton.SetActive(true);
           
         
            if (videoClipsIndex == 0)
            {
                isSetPass = false;
                videoUI.transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                videoUI.SetActive(false);
                senceObj.SetActive(true);
                cameraMgr.ShowCamera((int)videoPlayer.frame, index);
                animator.Play("Ani", -1, 1f);
                mainCamera.transform.position = cameraMgr.currentCamrea.transform.position;
                mainCamera.transform.rotation = cameraMgr.currentCamrea.transform.rotation;
                mainCamera.fieldOfView = cameraMgr.currentCamrea.GetComponent<Camera>().fieldOfView;
                mainCamera.nearClipPlane = cameraMgr.currentCamrea.GetComponent<Camera>().nearClipPlane;
                mainCamera.GetComponent<PostProcessingBehaviour>().profile = cameraMgr.currentCamrea.GetComponent<PostProcessingBehaviour>().profile;
            
            }
        }
    }
}

