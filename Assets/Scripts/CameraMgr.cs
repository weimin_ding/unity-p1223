﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMgr : MonoBehaviour
{

    [SerializeField]
    GameObject light;

    [SerializeField]
    GameObject[] cameras;

    public GameObject currentCamrea;

    [SerializeField]
    CharacterMgr characterMgr;

    public int index = 0;

    [SerializeField]
    string helpText;


    void NextCamera()
    {
        Debug.Log("NextCamera");
        if (index < cameras.Length - 1)
        {
            index++;
            Debug.Log(index);
            currentCamrea.SetActive(false);
            currentCamrea = cameras[index];
            currentCamrea.SetActive(true);
        }
    }

    void ActionEvent()
    {
        //FindObjectOfType<VideoMgr>().helpText.text = helpText;
    }

    void Light()
    {
        light.SetActive(true);
    }

   
    void NextCharacter()
    {
        index = 0;
        characterMgr.NextCharacter();

    }

    public void ShowCamera(int currentFrameIndex,int CharacterIndex)
    {
        if (CharacterIndex == 0)
        {
            if (currentFrameIndex < 93)
                index = 0;
            else if (currentFrameIndex < 164)
                index =1;
            else if (currentFrameIndex < 215)
                index = 2;
            else if (currentFrameIndex < 288)
                index = 3;
            else if (currentFrameIndex < 359)
                index = 4;
            else if (currentFrameIndex < 391)
                index = 5;
            else if (currentFrameIndex < 450)
                index = 6;
            else
                index = 7;
            currentCamrea = cameras[index];
        }
        else if (CharacterIndex == 1)
        {
            if (currentFrameIndex < 144)
                index = 0;
            else if (currentFrameIndex < 161)
                index = 1;
            else if (currentFrameIndex < 345)
                index = 2;
            else if (currentFrameIndex < 370)
                index = 3;
            else if (currentFrameIndex < 421)
                index = 4;
            else
                index = 5;
            currentCamrea = cameras[index];
        }

        else if (CharacterIndex == 2)
        {
            if (currentFrameIndex < 72)
                index = 0;
            else if (currentFrameIndex < 115)
                index = 1;
            else if (currentFrameIndex < 155)
                index = 2;
            else if (currentFrameIndex < 175)
                index = 3;
            else if (currentFrameIndex < 216)
                index = 4;
            else if (currentFrameIndex < 242)
                index = 5;
            else
                index = 6;
            currentCamrea = cameras[index];
        }

        else if (CharacterIndex == 3)
        {
            if (currentFrameIndex < 20)
                index = 0;
            else if (currentFrameIndex < 118)
                index = 1;
            else if (currentFrameIndex < 151)
                index = 2;
            else if (currentFrameIndex < 350)
                index = 3;
            else if (currentFrameIndex < 371)
                index = 4;
            else if (currentFrameIndex < 613)
                index = 5;
            else
                index = 6;
            currentCamrea = cameras[index];
        }

        else if (CharacterIndex == 4)
        {
            if (currentFrameIndex < 33)
                index = 0;
            else if (currentFrameIndex < 95)
                index = 1;
            else if (currentFrameIndex < 113)
                index = 2;
            else if (currentFrameIndex < 181)
                index = 3;
            else if (currentFrameIndex < 196)
                index = 4;
            else if (currentFrameIndex < 310)
                index = 5;
            else
                index = 6;
            currentCamrea = cameras[index];
        }
        else if (CharacterIndex == 5)
        {
            if (currentFrameIndex < 43)
                index = 0;
            else if (currentFrameIndex < 53)
                index = 1;
            else if (currentFrameIndex < 101)
                index = 2;
            else if (currentFrameIndex < 117)
                index = 3;
            else if (currentFrameIndex < 194)
                index = 4;
            else if (currentFrameIndex < 239)
                index = 5;
            else if (currentFrameIndex < 284)
                index = 6;
            else if (currentFrameIndex < 314)
                index = 7;
            else if (currentFrameIndex < 370)
                index = 8;
            else if (currentFrameIndex < 464)
                index = 9;
            else
                index = 10;
            currentCamrea = cameras[index];
        }
    }
}
