﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MicrophoneInput : MonoBehaviour
{

    public AudioSource audioSource;

    [SerializeField]
    AudioClip audioClip;

    float dustA = 1f;

    [SerializeField]
    Material dustM;

    [SerializeField]
    MeshRenderer dust;

    [SerializeField]
    ParticleSystem particle;

    public bool needKeyDown;

    private static int VOLUME_DATA_LENGTH = 128;    //录制的声音长度

    public float volume;        //音量

    public Text text;

    public Slider slider;

    private AudioClip mMicrophoneRecode;  //录制的音频
    private string mDeviceName;           //设备名称

    public float xishu = 1000;
    private const int frequency = 44100; //码率
    private const int lengthSec = 999;   //录制时长


    float time = 0;

    float CD = 2;

    bool canChange = false;

    bool isKeyDown = false;

    public bool canUse = false;

    // Use this for initialization
    void Start()
    {
        //获取设备名称
        mDeviceName = Microphone.devices[0];


        if (!needKeyDown)
        //录制一段音频
            mMicrophoneRecode = Microphone.Start(mDeviceName, true, lengthSec, frequency);
        dust.materials[0] = Instantiate(dustM) as Material;
        Color color = dust.materials[0].GetColor("_Color");
        dustA = color.a;
        Debug.Log(dustA);
    }

    // Update is called once per frame
    void Update()
    {
        //volume = GetMaxVolume();
        //volume *= xishu;
        slider.value = Mathf.Lerp(slider.value, volume / 10, 0.2f);
        text.text = volume.ToString("F2");

        if (volume > 0)
            volume -= Time.deltaTime;
        else if (volume < 0)
            volume = 0;

        if (!canUse)
            return;

        if (!canChange)
        {
            if (time < CD)
                time += Time.deltaTime;
            else
            {
                time = 0;
                canChange = true;
            }
        }
       

        if (!needKeyDown || (needKeyDown && isKeyDown))
        {
           
        }

        if (volume > 5)
        {
            if (volume > 6)
                volume = 6;
            ChuiHui();
        }
        else
        {
            if (particle.isPlaying)
                particle.Stop();
        }
    }

    void ChuiHui()
    {
        if (dustA >= 0)
        {
            if (dustA != 0)
            {
                Color color = dust.materials[0].GetColor("_Color");
                //dustA = color.r;
                Debug.Log(dustA);
                dustA -= Time.deltaTime * 0.5f;
                Debug.Log(dustA);
                if (dustA < 0)
                {
                    dustA = 0;
                    //audioSource.clip = audioClip;
                    //audioSource.Play();
                }
                dust.materials[0].SetColor("_Color", new Color(color.r, color.g, color.b, dustA));

            }
            if (!particle.loop)
                particle.loop = true;
            particle.Play();
        }
    }

    public void SetKeyDown(bool _isKeyDown)
    {
        isKeyDown = _isKeyDown;
    }

    public void SetVolume(float _volume)
    {
        volume = _volume;
    }


    /// <summary>
    /// 获取最大的音量
    /// </summary>
    /// 
    /// <returns>
    /// 音量大小
    /// </returns>
    private float GetMaxVolume()
    {
        float maxVolume = 0f;

        //用于储存一段时间内的音频信息
        float[] volumeData = new float[VOLUME_DATA_LENGTH];

        int offset;
        //获取录制的音频的开头位置
        offset = Microphone.GetPosition(mDeviceName) - VOLUME_DATA_LENGTH + 1;

        if (offset < 0)
        {
            return 0f;
        }

        //获取数据
        mMicrophoneRecode.GetData(volumeData, offset);

        //解析数据
        for (int i = 0; i < VOLUME_DATA_LENGTH; i++)
        {
            float tempVolume = volumeData[i];
            if (tempVolume > maxVolume)
            {
                maxVolume = tempVolume;
            }
        }

        return maxVolume;
    }
}