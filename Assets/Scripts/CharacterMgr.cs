﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMgr : MonoBehaviour
{
    [SerializeField]
    GameObject[] characters;

    public GameObject currentCharacter;

    public int index;

    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        if (characters != null && characters.Length > 0)
        {
            currentCharacter = characters[index];
            for (int i = 0; i < characters.Length; i++)
            {
                AnimationClip animationClip = characters[i].GetComponent<Animator>().runtimeAnimatorController.animationClips[0];
                AnimationEvent m_animator_End = new AnimationEvent();
                m_animator_End.functionName = "NextCharacter";
                m_animator_End.time = animationClip.length;
                animationClip.AddEvent(m_animator_End);
            }
        }
    }

    public void NextCharacter()
    {
        if (index < characters.Length-1)
        {
            index++;
            currentCharacter.SetActive(false);
            currentCharacter = characters[index];
            currentCharacter.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
