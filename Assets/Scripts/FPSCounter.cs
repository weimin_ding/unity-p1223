﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
	float deltaTime = 0.0f;

    [SerializeField]
    Text fpsText;
	
	void Update()
	{
        if (Time.timeScale == 0)
            return;

		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;

        fpsText.text = "FPS:" + fps.ToString("F0");
    }
}