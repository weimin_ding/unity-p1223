﻿using System;

[Serializable]
public class Payload
{

    public string id;
    public long seq;
    public string eventType;
    public string value;

    public Payload(string id, long seq, string eventType, string value)
    {
        this.id = id;
        this.seq = seq;
        this.eventType = eventType;
        this.value = value;
    }
}
