﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using BestHTTP.WebSocket;
using UnityEngine;
using WebSocketSharp.Server;


public class WSocketServer : MonoBehaviour
{
    public string url = "ws://localhost:9967/";
    private WebSocket webSocket;
    WebSocketServer wssv;
    [SerializeField]
    VideoMgr videoMgr;

    void Start()
    {
        //var wssv = new WebSocketServer("ws://localhost:8080/");
        wssv = new WebSocketServer(9967);
        wssv.AddWebSocketService<TestServer>("/");
        wssv.Start();
        init();
        Connect();
        //wssv.Stop();
    }

    private void init()
    {
        webSocket = new WebSocket(new Uri(url));
        webSocket.OnOpen += OnOpen;
        webSocket.OnMessage += OnMessageReceived;
        webSocket.OnError += OnError;
        webSocket.OnClosed += OnClosed;
    }

    private void antiInit()
    {
        webSocket.OnOpen = null;
        webSocket.OnMessage = null;
        webSocket.OnError = null;
        webSocket.OnClosed = null;
        webSocket = null;
    }


    public void Connect()
    {
        webSocket.Open();
    }

    private byte[] getBytes(string message)
    {

        byte[] buffer = Encoding.Default.GetBytes(message);
        return buffer;
    }

  
    public void Close()
    {
        webSocket.Close();
    }


    public void KeyEventMessage(string value)
    {
        if (value == "22")
            videoMgr.PlayOrPauseVideo();
        else if (value == "21")
            videoMgr.OpenOrCloseLight();
        else if (value == "19")
            videoMgr.ZoomIn();
        else if (value == "20")
            videoMgr.ZoomOut();
        else if (value == "7")
            videoMgr.SetText("0");
        else if (value == "8")
            videoMgr.SetText("1");
        else if (value == "9")
            videoMgr.SetText("2");
        else if (value == "10")
            videoMgr.SetText("3");
        else if (value == "11")
            videoMgr.SetText("4");
        else if (value == "12")
            videoMgr.SetText("5");
        else if (value == "13")
            videoMgr.SetText("6");
        else if (value == "14")
            videoMgr.SetText("7");
        else if (value == "15")
            videoMgr.SetText("8");
        else if (value == "16")
            videoMgr.SetText("9");
    }

    public void MotionEventMessage(string value)
    {
        string[] values = value.Split(',');
        if (values.Length < 3)
            return;
      
        if (values[0] == "0")
        {
            videoMgr.IntMontion(float.Parse(values[1]), float.Parse(values[2]));
        }
        else if (values[0] == "1")
            videoMgr.MouseUp();

        else if (values[0] == "2")
        {
            videoMgr.MouseDown();
            videoMgr.SetMontion(float.Parse(values[1]), float.Parse(values[2]));
        }
    }

    public void AmplitudeEventMessage(string valueStr)
    {
        Debug.Log(valueStr);
        float value = float.Parse(valueStr);
        if (value > 3)
        {
            Debug.Log(value);
            videoMgr.SetVolume(value);
        }
    }



    void TextEventMessage(string text)
    {

        string intNumber = Regex.Replace(text, @"[^\d]*", "");
        videoMgr.ShowMessage(intNumber);
    }


    #region WebSocket Event Handlers

    /// <summary>
    /// Called when the web socket is open, and we are ready to send and receive data
    /// </summary>
    void OnOpen(WebSocket ws)
    {
        Debug.Log("connected");
    }

    /// <summary>
    /// Called when we received a text message from the server
    /// </summary>
    void OnMessageReceived(WebSocket ws, string message)
    {
     
        Payload payload = JsonUtility.FromJson<Payload>(message);
        Debug.Log(payload.eventType);
        switch (payload.eventType)
        {
            case "KeyEvent":
                KeyEventMessage(payload.value);
                break;
            case "MotionEvent":
                MotionEventMessage(payload.value);
                break;
            case "AmplitudeEvent":
                AmplitudeEventMessage(payload.value);
                break;
            case "TextEvent":
                TextEventMessage(payload.value);
                break;
        }
    }

    /// <summary>
    /// Called when the web socket closed
    /// </summary>
    void OnClosed(WebSocket ws, UInt16 code, string message)
    {
        Debug.Log(message);
        antiInit();
        init();
    }

    private void OnDestroy()
    {

        wssv.Stop();

        if (webSocket != null && webSocket.IsOpen)
        {
            webSocket.Close();
            antiInit();
        }
    }

    /// <summary>
    /// Called when an error occured on client side
    /// </summary>
    void OnError(WebSocket ws, Exception ex)
    {
        string errorMsg = string.Empty;
#if !UNITY_WEBGL || UNITY_EDITOR
        if (ws.InternalRequest.Response != null)
            errorMsg = string.Format("Status Code from Server: {0} and Message: {1}", ws.InternalRequest.Response.StatusCode, ws.InternalRequest.Response.Message);
#endif
        Debug.Log(errorMsg);
        antiInit();
        init();
    }

    #endregion
}
